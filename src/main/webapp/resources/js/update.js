var uri;
var port;
var websocketSession;

function init() {
 if (document.location.protocol.indexOf('s') > -1) {
  uri = 'wss://';
 } else {
  uri = 'ws://';
 }

 if (document.location.host.indexOf('uptitek') > -1) {
  port = ':7844';
 } else {
  port = '';
 }

 uri = uri + document.location.host + port + '/sms/update';
 console.log(uri);


 if (!websocketSession) {
  websocketSession = new WebSocket(uri);
 }

 websocketSession.onopen = function () {
  websocketSession.send("Opened");
 };
 websocketSession.onerror = function (e) {
 };

 websocketSession.onerror = function (e) {
 };

 websocketSession.onmessage = function (e) {
  var data = e.data; //May use conditions to update UI based on value of data
  var trigger = document.getElementById('trigger');
  trigger.click();
 };
}
document.addEventListener("DOMContentLoaded", init, false);