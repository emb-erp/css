package css;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

/**
 *
 * @author bu_000
 */
@Named
@SessionScoped
public class Css implements Serializable{

    private Date date;
    private Tax item;
    private List<Tax> items = null;
    private List<Tax> complete = null;

    @PostConstruct
    public void init() {
        findAll();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Tax getItem() {
        return item;
    }

    public void setItem(Tax item) {
        this.item = item;
    }

    public List<Tax> getComplete() {
        return complete;
    }

    public void setComplete(List<Tax> complete) {
        this.complete = complete;
    }

    public List<Tax> getItems() {
        return findAll();
    }

    public void setItems(List<Tax> items) {
        this.items = items;
    }

    public List<Tax> complete(String query) {
        if (complete == null) {
            complete = findAll();
        }
        List<Tax> suggestions = new ArrayList<>();
        for (Tax p : complete) {
            if ((p.sort().toLowerCase()).contains(query.toLowerCase()) && !suggestions.contains(p)) {
                suggestions.add(p);
            }
        }
        return suggestions;
    }

    public Tax prepareCreate() {
        return new Tax();
    }

    public Tax prepareUpdate() {
        return item;
    }

    public Tax prepareEdit() {
        return item;
    }

    public Tax prepareDelete() {
        return item;
    }

    public Tax prepareMoreInfo() {
        return item;
    }

    private List<Tax> findAll() {
        items = new ArrayList<>();
        items.add(new Tax(1, "Job seekers", 16.0));
        items.add(new Tax(2, "Maternity", 21.5));
        items.add(new Tax(3, "Low Income", 34.5));
        items.add(new Tax(4, "Mid Income", 34.5));
        items.add(new Tax(5, "High Income", 46.0));
        return items;
    }

    public Tax getTax(java.lang.Integer id) {
        items = findAll();
        for (Iterator<Tax> it = findAll().iterator(); it.hasNext();) {
            Tax t = it.next();
            if (Objects.equals(t.getId(), id)) {
                return t;
            }
        }
        return new Tax(1, "Job seekers", 16.0);
    }

    @FacesConverter(forClass = Tax.class)
    public static class CssConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            Css css = (Css) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "css");
            return css.getTax(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Tax) {
                Tax o = (Tax) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).
                        log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}",
                                new Object[]{object, object.getClass().getName(), Tax.class.getName()});
                return null;
            }
        }

    }

}
