package css;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Transient;

/**
 *
 * @author b
 */
public class Tax implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String allowance;
    private double allowanceRate;
    @Transient
    private String identifier;
    @Transient
    private boolean selected;

    public String getIdentifier() {
        return String.valueOf(allowance);
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String sort() {
        return String.valueOf(allowance);
    }

    public Tax() {
    }

    public Tax(Integer id) {
        this.id = id;
    }

    public Tax(Integer id,
            String allowance,
            double allowanceRate) {
        this.id = id;
        this.allowance = allowance;
        this.allowanceRate = allowanceRate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAllowance() {
        return allowance;
    }

    public void setAllowance(String allowance) {
        this.allowance = allowance;
    }

    public double getAllowanceRate() {
        return allowanceRate;
    }

    public void setAllowanceRate(double allowanceRate) {
        this.allowanceRate = allowanceRate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tax other = (Tax) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return getIdentifier();
    }

}
